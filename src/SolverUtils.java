
public class SolverUtils {
	
	private static final int GRID_SIZE = 9;
			
	private static boolean isNumberInRow(int[][] board, int number, int row) {
		for(int i = 0; i < GRID_SIZE; i++) {
			if(board[row][i] == number) {
				return true;
			}
		}
		return false;
	}
	
	private static boolean isNumberInColumn(int[][] board, int number, int column) {
		for(int i = 0; i < GRID_SIZE; i++) {
			if(board[i][column] == number) {
				return true;
			}
		}
		return false;
	}
	
	private static boolean isNumberInGrid(int[][] board, int number, int row, int column) {
			int localGridRow = row - (row % 3);
			int localGridColumn = column - (column % 3);
			
			for(int i = localGridRow; i < localGridRow + 3; i++) {
				for(int j = localGridColumn; j < localGridColumn + 3; j++) {
					if(board[i][j] == number) {
						return true;
					}
				}
			}
			return false;
	}

	private static boolean isValidPlacement(int[][] board, int number, int row, int column) {
		return !isNumberInRow(board, number, row) &&
				!isNumberInColumn(board, number, column) &&
				!isNumberInGrid(board, number, row, column);
	}
	
	public static boolean solveBoard(int[][] board) {
		for(int row = 0 ; row < GRID_SIZE; row++) {
			for(int column = 0; column < GRID_SIZE; column++) {
				if(board[row][column] == 0) {
					for(int candidate = 1; candidate <= GRID_SIZE; candidate++) {
						if(isValidPlacement(board, candidate, row, column)) {
							board[row][column] = candidate;							
							if(solveBoard(board)){
								return true;
							}
							else {
								board[row][column] = 0;
							}
						}
					}
					return false;
				}
			}
		}
		return true;		
	}
	
	public static void printBoard(int[][] board) {
		for(int row = 0; row < GRID_SIZE; row++) {
			if(row % 3 == 0 && row!= 0) {
				System.out.println("----------");
			}
			for(int column = 0; column < GRID_SIZE; column++) {
				if(column % 3 == 0 && column!= 0) {
					System.out.print("|");
				}
				System.out.print(board[row][column]);
			}
			System.out.println();
		}
	}
	
	public static void printBreak() {
		System.out.println("===========");
	}
	
}
